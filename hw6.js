    // Сегодня мы печем пироги!

    // Вам нужно испечь по 10 пирогов каждого типа
    // Можно использовать все знания, которые вы получили на наших лекциях

    // Рецепты:
    // Яблочный пирог (3 кг яблок, 2 кг. муки, 1 литр молока, 3 яйца)
    // Клубничный пирог (5 кг клубники, 1 кг. муки, 2 литр молока, 4 яйца)
    // Абрикосовый пирог (2 кг абрикос, 3 кг. муки, 2 литр молока, 2 яйца)
    // Запасы на складе

    var apples = 20; // Яблоки, кг
    var strawberry = 20; // Клубника - 75 р/кг
    var apricot = 20; // Абрикос - 35 р/кг
    var flour = 20; // Мука - 10 р/кг
    var milk = 20; // Молоко - 15 р/литр
    var eggs = 50; // Яйца - 3 р/шт
    var applePie = 0; // количество яблочных пирогов, которое испекли.
    var strawberryPie = 0; // количество клубничных пирогов, которое испекли.
    var apricotPie = 0; // количество абрикосовых пирогов, которое испекли.

// Задание 1

    function pies(kind, number) {
      for (var i = 0; i < number; i++) {
        switch (kind) {
        case 'apples':
          makeApplePie();
          break;
        case 'strawberry':
          makeStrawberryPie();
          break;
        case 'apricot':
          makeApricotPie();
          break;
        }
      }
    }
    function messeng(ingredient) {
      var declension, name;
      switch (ingredient) {
      case 'apples':
        name= 'яблоки';
        declension= 'Кончились';
        apples += 10;
        break;
      case 'strawberry':
        name= 'клубника';
        declension= 'Кончилась';
        strawberry += 10;
        break;
      case 'apricot':
        name= 'абрикосы';
        declension= 'Кончились';
        apricot += 10;
        break;
      case 'flour':
        name= 'мука';
        declension= 'Кончилась';
        flour += 10;
        break;
      case 'milk':
        name= 'молоко';
        declension= 'Кончилось';
        milk += 10;
        break;
      case 'eggs':
        name= 'яйца';
        declension= 'Кончились';
        eggs += 10;
        break;
      }
      console.log(''+declension+' '+name+'! Купили еще 10 кг');

    }



     // Печем яблочный пироги:
    function makeApplePie () {
      if (apples - 3 < 0) {
        messeng('apples');
      }
        if (flour - 2 < 0) {
        messeng('flour');
      }
        if (milk - 1 < 0) {
        messeng('milk');
      }
        if (eggs - 3 < 0) {
        messeng('eggs');
      }
      apples -= 3;
      flour -= 2;
      milk -= 1;
      eggs -= 3;
      applePie++;
      console.log('Яблочный пирог ' +applePie+ ' готов!');
    }

    // Печем абрикосовые пироги:
    function makeApricotPie () {
      if (apricot - 2 < 0) {
        messeng('apricot');
      }
        if (flour - 2 < 0) {
        messeng('flour');
      }
        if (milk - 1 < 0) {
        messeng('milk');
      }
        if (eggs - 3 < 0) {
        messeng('eggs');
      }
      apricot -= 2
      flour -= 3
      milk -= 2
      eggs -= 2
      apricotPie++
      console.log('Абрикосовый пирог '+apricotPie+' готов!');
    }

    // Печем клубничные пироги:
    function makeStrawberryPie () {
      if (strawberry - 5 < 0) {
        messeng('strawberry');
      }
        if (flour - 2 < 0) {
        messeng('flour');
      }
        if (milk - 1 < 0) {
        messeng('milk');
      }
        if (eggs - 3 < 0) {
        messeng('eggs');
      }
      strawberry -= 5
      flour -= 1
      milk -= 2
      eggs -= 4
      strawberryPie++
      console.log('Клубничный пирог '+strawberryPie+' готов!');
    }

    pies('apples', 10);
    pies('apricot', 10);
    pies('strawberry', 10);

// Задание 2
//При помощи циклов
  function sumToCycle(n) {
    for (var sum=0, i = 0; i <= n; i++) {
      sum += i;
    }
    return(sum);
  }
  console.log(sumToCycle(3));

// При помощи рекурсии
  function sumToRecurs(n) {
    if (n == 1) {
      return n;
    }
    else {
      return n + sumToRecurs(n-1);
    }
  }
  console.log(sumToRecurs(3));

 // По моему мнению решение задачи при помощи циклов лучше рекурсии. При помощи циклов операции выполняються по более простой схеме, рекурсия же для меня сложна в понимании как что происходит. К тому же цикл не грузит стек.
